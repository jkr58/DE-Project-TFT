#!/usr/bin/env python3

import  requests, re, pandas as pd, urllib
from collections import defaultdict
from bs4 import BeautifulSoup

url = "https://lolchess.gg/statistics/meta/deck/MA"
test = urllib.request.urlopen(url).read()
testsoup = BeautifulSoup(test, 'html.parser')

# region, galaxy, name, champstring
regionlist = []
galaxylist = []
namelist = []
champstringlist = []
timelist = []
agelist = []

from time import gmtime, strftime
rightnow = strftime("%Y-%m-%d %H:%M:%S", gmtime())

for deckitem in testsoup.find_all("div",{"class":"deckItem"}):
    #class : region
    matchinfo = deckitem.find_all("div",{"class":"match-info"})
    text = re.sub(r"\n",'',matchinfo[0].text) #remove \n
    region = re.findall(r"[A-Z]{2,3}", text) #get region
    regionlist.append(region[0])
#     print(type(region[0]))
    galaxy = re.sub(r"\s",'',re.findall(r"[A-Za-z]{4}.*", text)[0]) #get galaxy
    galaxylist.append(galaxy)
    #class : summoner
    summoner = deckitem.find_all("div",{"class":"summoner"})[0].text
    name = ''.join(re.findall(r"[^\n\s]",summoner))
    namelist.append(name)
    #class : units
    champlist = []
    for units1 in deckitem.find_all("div",{"class":"units"}):
        for units2 in units1.find_all("div",{"class":"unit"}):
            ele2 = units2.find_all('img',{'alt':True})
            champlist.append(re.search( r"\"(.*?)\"",str(ele2[0])).groups()[0])
    champstring = ','.join(champlist)
    champstringlist.append(champstring)
    #class : currenttime
    timelist.append(rightnow)
    #class : age
    t = deckitem.find_all("div",{"class":"age"})[0].text
    n = re.findall(r"\d", t)
    i = int(''.join(n))
    agelist.append(i)

data = pd.DataFrame({"region":regionlist,"name":namelist,"galaxy":galaxylist,
                     "units":champstringlist, "current_time":timelist, "age":agelist})

print(data.head())