# A dockerfile must always start by importing the base image.
# We use the keyword 'FROM' to do that.
# In our example, we want import the python image.
# So we write 'python' for the image name and 'latest' for the version.
FROM python:latest

RUN pip install requests

RUN pip install pandas

RUN pip install bs4

COPY TFTscraper.py /

CMD [ "python", "./TFTscraper.py" ]